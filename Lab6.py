import RPi.GPIO as IO
import time                            #calling time to provide delays in program

#IO.setwarnings(False)           #do not show any warnings
IO.setmode(IO.BCM)

IO.setup(18,IO.OUT)           # initialize GPIO19 as an output.
IO.setup(17,IO.OUT)
p1 = IO.PWM(18,100)
p2 = IO.PWM(17,100)
			         #GPIO19 as PWM output, with 100Hz frequency
p1.start(0)                              #generate PWM signal with 0% duty cycle
p2.start(0)


def rotateRight(n):
	for x in range(n):
		p1.ChangeDutyCycle(50)
		p2.ChangeDutyCycle(50)
		time.sleep(.1)
def forward():
	print("Moving forward")
	for x in range(12):
		p1.ChangeDutyCycle(12)	#Right wheel  1-14 = Forward 15,20,50, 99 (slow) = Backward 1 Fastest for forward.
		p2.ChangeDutyCycle(50)	#Left wheel
		time.sleep(.1)

def backward():
	print("Moving backward")
	for x in range(12):
		p1.ChangeDutyCycle(50)
		p2.ChangeDutyCycle(12)
		time.sleep(.1)
try:
		forward()
		time.sleep(1)
		backward()
		time.sleep(1)
except KeyboardInterrupt:
		IO.cleanup()